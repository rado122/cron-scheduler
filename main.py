"""
CLI entry point

TODO: add validators for the input
TODO: convert main into a function that accepts raise_on_exc param for validators behavior(silent vs exception pass)
TODO: Add setup.py file with main function as an entry point
TODO: Add unittests and ensure 100% cov on Scheduler._schedule as it contains a lot of ifs
TODO: Write a better README.md file
"""

import sys
from cron_scheduler.data_classes import Time
from cron_scheduler.scheduler import Schedule


def get_stdin_jobs():
    return [line.rstrip('\n') for line in sys.stdin.readlines()]


if __name__ == '__main__':
    arguments = sys.argv
    now_str = arguments[1]
    now = Time.from_str(now_str)
    jobs = get_stdin_jobs()

    for job in jobs:
        schedule = Schedule(now, job)
        print(schedule)


