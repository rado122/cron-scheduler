from dataclasses import dataclass


@dataclass
class Time:
    """
    A dataclass that represents time
    """
    hour: int = None
    minute: int = None

    @classmethod
    def from_str(cls, string):
        """
        Instantiates a Time class out of a 'HH:MM' formatted string
        :param string: 'HH:MM' formatted string
        :return:
        """
        hour, minute = (int(num) for num in string.split(':'))
        return cls(hour=hour, minute=minute)

    def __str__(self):
        return f"{self.hour}:{self.minute if self.minute != 0 else '00'}"

    def __repr__(self):
        return self.__str__()
