"""
A module containing the schedule class for scheduling tasks

TODO: document better the format of job_expr
TODO: Extract scheduling cases into functions to increase readability
TODO: Might sanitise cron input into a dataclass with minute, hour properties where * is represented as None
"""

from cron_scheduler.data_classes import Time


class Schedule:

    def __init__(self, now: Time, job_expr: str):
        """

        :param now: A time object representing the time now
        :param job_expr: A cron like schedule expression
        """
        self.now = now
        self.job_expr = job_expr
        self.today = True
        self.schedule_time = Time()
        self._schedule()

    def _schedule(self):
        """
        Internal function that schedules next execution time
        """
        cron_minute, cron_hour, self.task = self.job_expr.split(' ')

        if '*' not in (cron_hour, cron_minute):
            # regular job
            self.schedule_time = Time(hour=int(cron_hour), minute=int(cron_minute))
            if self.now.hour > int(cron_hour) or self.now.minute > int(cron_minute):
                self.today = False
        
        elif cron_hour == '*' and cron_minute != '*':
            # flexible hour strict minute

            self.schedule_time.minute = cron_minute
            if self.now.minute <= int(cron_minute):
                self.schedule_time.hour = self.now.hour
            else:
                if self.now.hour < 23:
                    self.schedule_time.hour = self.now.hour + 1
                else:
                    self.today = False
                    self.schedule_time.hour = 0

        elif cron_hour != '*' and cron_minute == "*":
            # flexible minute strict hour

            self.schedule_time.hour = cron_hour
            self.schedule_time.minute = 0
            if self.now.hour == int(cron_hour):
                self.schedule_time.minute = self.now.minute
            elif self.now.hour > int(cron_hour):
                self.today = False

        else:
            # both wildcard

            self.schedule_time = self.now

    def __str__(self):
        return f"{self.schedule_time} {'today' if self.today else 'tomorrow'} {self.task}"

    def __repr__(self):
        return self.__str__()