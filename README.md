# Cron scheduler
{Placeholder for really awesome description}
### Requirements
- Python 3.7+
- Module has no other deps

### How to run
`$ python3 main.py {HH:MM} < {input_file}`

### Example usage:

1. call: `$ python3 main.py 22:33 < config.example`

2. output:
```
1:30 tomorrow /bin/run_me_daily
22:45 today /bin/run_me_hourly
22:33 today /bin/run_me_every_minute
19:00 tomorrow /bin/run_me_sixty_times
22:33 today /bazingaa
```
